/*Package dynobject is part of gopilot
Copyright (C) 2020 by Martin Langlotz aka stackshadow

This file is part of gopilot, an rewrite of the copilot-project in go

gopilot is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, version 3 of this License

gopilot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with gopilot.  If not, see <http://www.gnu.org/licenses/>.
*/
package dynobject

// Action represents an action inside an editor
type Action struct {

	// Name is the actionname ( must be translated !!! ) ( example: delete, save, connect, disconnect, test )
	Name string `json:"name"`

	// Command which will be send when the action is triggered
	Command string `json:"command"`

	// Response ( optional ) the response-command, when the action was successful
	Response string `json:"response"`
}
