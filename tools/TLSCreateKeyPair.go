/*Package tools is part of gopilot
Copyright (C) 2020 by Martin Langlotz aka stackshadow

This file is part of gopilot, an rewrite of the copilot-project in go

gopilot is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, version 3 of this License

gopilot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with gopilot.  If not, see <http://www.gnu.org/licenses/>.
*/
package tools

import (
	"os"
	"os/exec"

	"github.com/sirupsen/logrus"
)

// TLSCreateKeyPair create a new TLS-Key-Pair for you
// basePath is the base-path ( without *.key and *.crt )
func TLSCreateKeyPair(basePath, fileName, commonName string) (KeyFileName string, CertFileName string, Error error) {

	keyFileName := basePath + "/" + fileName + ".key"
	certFileName := basePath + "/" + fileName + ".crt"

	// check basepath
	if baseFileInfo, _ := os.Stat(basePath); baseFileInfo == nil {
		err := os.MkdirAll(basePath, os.ModePerm)
		if err != nil {
			return "", "", err
		}
	}

	if keyFileInfo, _ := os.Stat(keyFileName); keyFileInfo == nil {

		cmdKey := exec.Command("openssl", "ecparam", "-genkey", "-name", "secp384r1",
			"-out", keyFileName,
		)

		logrus.WithFields(logrus.Fields{"keyFile": keyFileName, "cmd": cmdKey.Args}).Debug("Create a new key")
		errKey := cmdKey.Run()
		if errKey != nil {
			logrus.WithFields(logrus.Fields{"keyFile": keyFileName}).Error(errKey)
			return "", "", errKey
		}

		logrus.WithFields(logrus.Fields{"keyFile": keyFileName}).Debug("Created a new key")
	} else {
		logrus.WithFields(logrus.Fields{"keyFile": keyFileName}).Debug("Key exist")
	}

	if certFileInfo, _ := os.Stat(certFileName); certFileInfo == nil {

		logrus.WithFields(logrus.Fields{"certFile": certFileName}).Debug("Create a new certificate")
		cmdCRT := exec.Command("openssl", "req", "-new", "-x509", "-sha256",
			"-key", keyFileName,
			"-out", certFileName,
			"-days", "3650",
			"-subj", "/C=DE/ST=UNKNOWN/L=UNKNOWN/O=COPILOTD/OU=DAEMON/CN="+commonName,
		)
		errCRT := cmdCRT.Run()
		if errCRT != nil {
			logrus.WithFields(logrus.Fields{"certFile": certFileName}).Error(errCRT)
			return "", "", errCRT
		}

		logrus.WithFields(logrus.Fields{"certFile": certFileName}).Debug("Created a new certificate")
	} else {
		logrus.WithFields(logrus.Fields{"certFile": certFileName}).Debug("Certificate exist")
	}

	return keyFileName, certFileName, nil
}
