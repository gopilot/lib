package tools

import (
	"testing"
)

func TestWhereis(t *testing.T) {

	// try to find sh
	output := Whereis("sh")
	if output == "" {
		t.FailNow()
		return
	}

}

func TestShouldNotExist(t *testing.T) {

	// try to find sh
	output := Whereis("asdaegfcsertc")
	if output != "" {
		t.FailNow()
		return
	}

}
