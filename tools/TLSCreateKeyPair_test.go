/*Package tools is part of gopilot
Copyright (C) 2020 by Martin Langlotz aka stackshadow

This file is part of gopilot, an rewrite of the copilot-project in go

gopilot is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, version 3 of this License

gopilot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with gopilot.  If not, see <http://www.gnu.org/licenses/>.
*/
package tools

import (
	"os"
	"path/filepath"
	"testing"
)

func TestTLSCreateKeyPair(t *testing.T) {

	// local config directory
	ex, err := os.Executable()
	if err != nil {
		t.FailNow()
		return
	}
	exPath := filepath.Dir(ex)

	keyfile, certfile, _ := TLSCreateKeyPair(exPath, "integrationtest", "integration")

	if fileInfo, _ := os.Stat(keyfile); fileInfo == nil {
		t.FailNow()
		return
	}
	if fileInfo, _ := os.Stat(certfile); fileInfo == nil {
		t.FailNow()
		return
	}

	// this must fail
	_, _, err = TLSCreateKeyPair("/dev", "integrationtest", "integration")
	if err == nil {
		t.FailNow()
		return
	}

	_, _, err = TLSCreateKeyPair("/deva", "integrationtest", "integration")
	if err == nil {
		t.FailNow()
		return
	}
}
