package tools

import (
	"os/exec"
	"strings"

	"github.com/sirupsen/logrus"
)

// Whereis return the full-path of an binary
func Whereis(name string) string {
	cmd := exec.Command("whereis", name)

	stdoutStderr, err := cmd.CombinedOutput()
	if err != nil {
		logrus.Fatal(err)
		return ""
	}

	// trim paths
	output := strings.Trim(string(stdoutStderr), " \n\t")

	values := strings.Split(output, ":")
	if len(values) < 2 {
		logrus.Errorf("Result error")
		return ""
	}

	// the first element should be the name we are looking for
	if values[0] != name {
		logrus.Errorf("values[0] (%s) != %s", values[0], name)
		return ""
	}

	// if the second element is " " nothing was found
	if values[1] == "" {
		return ""
	}

	// split execs
	paths := strings.Trim(values[1], " \n\t")
	elements := strings.Split(paths, " ")
	if len(elements) < 1 {
		logrus.Errorf("Nothing found")
		return ""
	}

	return string(string(elements[0]))
}
