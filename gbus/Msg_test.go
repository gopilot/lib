/*Package gbus is part of gopilot
Copyright (C) 2020 by Martin Langlotz aka stackshadow

This file is part of gopilot, an rewrite of the copilot-project in go

gopilot is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, version 3 of this License

gopilot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with gopilot.  If not, see <http://www.gnu.org/licenses/>.
*/
package gbus

import (
	"testing"
)

func TestContext(t *testing.T) {

	message := MsgNew(0, "test")

	if message.ContextGet() != "test" {
		t.FailNow()
		return
	}

}

func TestConversion(t *testing.T) {

	var message Msg
	messageString, err := message.ToJSONString()
	if err != nil {
		t.FailNow()
		return
	}

	if messageString != `{"s":"","sg":"","t":"","tg":"","c":"","v":""}` {
		t.FailNow()
		return
	}

	byteString, err := message.ToJSONByteArray()
	if err != nil {
		t.FailNow()
		return
	}

	if string(byteString) != `{"s":"","sg":"","t":"","tg":"","c":"","v":""}` {
		t.FailNow()
		return
	}
}
