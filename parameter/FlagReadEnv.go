/*Package parameter is part of gopilot
Copyright (C) 2020 by Martin Langlotz aka stackshadow

This file is part of gopilot, an rewrite of the copilot-project in go

gopilot is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, version 3 of this License

gopilot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with gopilot.  If not, see <http://www.gnu.org/licenses/>.
*/
package parameter

import (
	"flag"

	"os"
	"strings"

	"gitlab.com/gopilot/lib/log"
)

// FlagsFromEnv will read command-line-flags from environment
//
// Every flag-name in upper-case will be readed from environment
func FlagsFromEnv() {
	flag.VisitAll(readEnvVarsFromFlag)
}

func readEnvVarsFromFlag(flag *flag.Flag) {

	// get the flag name
	flagName := flag.Name
	flagName = strings.ToUpper(flagName)
	flagName = strings.Replace(flagName, " ", "_", -1)
	flagName = strings.Replace(flagName, ".", "_", -1)

	// read from environment
	envVar, exist := os.LookupEnv(flagName)
	if exist == true {
		log.Logger.Debug().Str("flag", flag.Name).Str("env", flagName).Msg("Set flag-var from environment variable")
		flag.DefValue = envVar
		flag.Value.Set(envVar)
	}

}
