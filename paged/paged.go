/*Package paged is part of gopilot
Copyright (C) 2020 by Martin Langlotz aka stackshadow

This file is part of gopilot, an rewrite of the copilot-project in go

gopilot is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, version 3 of this License

gopilot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with gopilot.  If not, see <http://www.gnu.org/licenses/>.
*/
package paged

// Data represents paged data
type Data struct {

	// Page is the current page ( 0 == first page )
	Page int64 `json:"page"`

	// Size represents the maximum pages ( 0 == no page available )
	Size int64 `json:"size"`

	// Data represents the payload
	Data interface{} `json:"data"`
}

// Request represents an apged request
type Request struct {
	// Type represents the type of paged data
	// for example "minimal" for minimal data or "full" for everything
	Type string `json:"type,omitempty"`

	// Page
	Page int64 `json:"page"`

	// Element per page
	ElementCount int64 `json:"elementCount"`

	// id
	Data string `json:"data"`
}
