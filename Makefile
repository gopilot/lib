
deps:
	GOPATH=${HOME}/go:${PWD} \
	go get go.mongodb.org/mongo-driver/mongo \
	go get -d -v ...

build:
	GOPATH=${HOME}/go:${PWD} \
	go build ...

test-config:
	cd config && go test -coverprofile coverage.out .
	cd config && go tool cover -html=coverage.out -o coverage.html

test-gbus:
	cd gbus && go test -coverprofile coverage.out .
	cd gbus && go tool cover -html=coverage.out -o coverage.html

test-tools:
	cd tools && go test -coverprofile coverage.out .
	cd tools && go tool cover -html=coverage.out -o coverage.html

test-socket:
	cd socket && go test -v -coverprofile coverage.out .
	cd socket && go tool cover -html=coverage.out -o coverage.html


test:
	rm -f coverage.out coverage.html
	go test $$(go list ./... | grep -v /vendor/ | grep -v mynodename | grep -v gbus | grep -v tools | grep -v clog ) -coverprofile coverage.out
	go tool cover -html=coverage.out -o coverage.html
	rm coverage.out




clean:
	find . -name "*coverage.out" -delete -print
	find . -name "*coverage.html" -delete -print
	find . -name "*debug.test" -delete -print

~/go/bin/gocyclo:
	go install github.com/fzipp/gocyclo
cyclo: ~/go/bin/gocyclo
	~/go/bin/gocyclo -avg .


~/go/bin/golint:
	go get -u golang.org/x/lint/golint
golint: ~/go/bin/golint
	/home/theia/go/bin/golint ./...

vet:
	go vet ./...

update:
	go list -u -m all
	go mod tidy