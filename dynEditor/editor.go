/*Package dyneditor is part of gopilot
Copyright (C) 2020 by Martin Langlotz aka stackshadow

This file is part of gopilot, an rewrite of the copilot-project in go

gopilot is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, version 3 of this License

gopilot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with gopilot.  If not, see <http://www.gnu.org/licenses/>.
*/
package dyneditor

import (
	"encoding/json"
)

// Editor represent an editor
type Editor struct {
	ID     string  `json:"id"`
	Inputs []Input `json:"inputs"`
}

// EditorRequestPayload represents the payload to get an editor from the backend
type EditorRequestPayload struct {
	ID   string `json:"id"`
	Type string `json:"type,omitempty"`
}

// NewEditor just create a new editor
func NewEditor() Editor {
	return Editor{}
}

// AddInput add a new input-field
func (e *Editor) AddInput(name string, inputType string, displayName string, hint string) *Editor {

	// create a new input and add it to fields
	input := NewInput(name, inputType, displayName, hint)
	e.Inputs = append(e.Inputs, input)

	return e

}

// GetInput return the Input-Object or nil if field don't exist
func (e *Editor) GetInput(name string) *Input {

	for index, input := range e.Inputs {

		if input.Name == name {
			return &e.Inputs[index]
		}

	}

	return nil
}

// SetPrimaryFieldName will set the primary field name
func (e *Editor) SetPrimaryFieldName(primaryFieldName string) *Editor {

	for index, input := range e.Inputs {

		if input.Name == primaryFieldName {
			input.SetMandatory(true)
			e.Inputs[index] = input
		}

	}

	return e
}

// ToJSON will convert the editor to json
func (e *Editor) ToJSON() string {

	bytes, err := json.Marshal(e)
	if err != nil {
		return "{}"
	}

	return string(bytes)
}
