/*Package dyneditor is part of gopilot
Copyright (C) 2020 by Martin Langlotz aka stackshadow

This file is part of gopilot, an rewrite of the copilot-project in go

gopilot is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, version 3 of this License

gopilot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with gopilot.  If not, see <http://www.gnu.org/licenses/>.
*/
package dyneditor

// Input represents a single editor-input
type Input struct {
	Name string `json:"name"`
	Type string `json:"type"`

	DisplayName string `json:"displayName"`
	Hint        string `json:"hint"`

	Visible        bool `json:"visible"`
	ValueMandatory bool `json:"valueMandatory"`
	ValueHidden    bool `json:"valueHidden"`
	ValueReadOnly  bool `json:"valueReadOnly"`

	Options []InputOption `json:"options"`
}

// InputOption represent a single option for drop-down-boxes
type InputOption struct {
	Value       string `json:"value"`
	DisplayName string `json:"displayName"`
}

// NewInput create a new editor-input
func NewInput(name string, inputType string, displayName string, hint string) Input {

	var newInput Input
	newInput.Name = name
	newInput.Type = inputType

	newInput.DisplayName = displayName
	newInput.Hint = hint

	newInput.Visible = true

	return newInput
}

// IsString return true if Input-Field is a string
func (i *Input) IsString() bool {
	return i.Type == "string"
}

// AddOption will add an Option
func (i *Input) AddOption(displayName, value string) *Input {

	newOption := InputOption{
		DisplayName: displayName,
		Value:       value,
	}

	i.Options = append(i.Options, newOption)

	return i
}

// SetVisible set if the field is visible in the editor itselfe
func (i *Input) SetVisible(visible bool) *Input {
	i.Visible = visible
	return i
}

// SetMandatory set the permanent state of an input
//
// This force the sending of this value
func (i *Input) SetMandatory(permanent bool) *Input {
	i.ValueMandatory = permanent
	return i
}

// SetHidden will set the hidden-state of an field
//
// This don't hide the field, it hide the field values ( like passwords )
func (i *Input) SetHidden(hidden bool) *Input {
	i.ValueHidden = hidden
	return i
}

// SetReadOnly will set the field to readonly if true
func (i *Input) SetReadOnly(readonly bool) *Input {
	i.ValueReadOnly = readonly
	return i
}
