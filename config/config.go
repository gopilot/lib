/*Package config is part of gopilot
Copyright (C) 2020 by Martin Langlotz aka stackshadow

This file is part of gopilot, an rewrite of the copilot-project in go

gopilot is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, version 3 of this License

gopilot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with gopilot.  If not, see <http://www.gnu.org/licenses/>.
*/
package config

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/json"
	"errors"
	"io"

	"github.com/sirupsen/logrus"

	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
)

// File represents a configfile
type File struct {
	log      *logrus.Entry
	fileName string
	entrys   map[string]interface{}
}

// New create an new empty config File-Object
//
// This don't read the file itselfe, you need to call File.Read() after it
//
// This function check if the config-file exist
//
// - on the executable path
//
// - on UNIX at /etc/gopilot
func New(fileName string) *File {

	var newConfigFile = File{
		log: logrus.WithFields(
			logrus.Fields{
				"prefix": "CONFIG",
			},
		),
		entrys: make(map[string]interface{}),
	}

	var configPath string

	// local config-directory
	ex, err := os.Executable()
	if err != nil {
		return nil
	}
	exPath := filepath.Dir(ex)
	configPath = exPath + "/" + fileName + ".json"
	// here we don't check if the file exist
	// its the default path
	// if _, err := os.Stat(configPath); os.IsExist(err) {
	newConfigFile.fileName = configPath
	// }

	// global config-directory
	configPath = "/etc/gopilot/" + fileName + ".json"
	if _, err := os.Stat(configPath); os.IsExist(err) {
		newConfigFile.fileName = configPath
	}

	// debug
	newConfigFile.log.Debug("Config-File is '" + newConfigFile.fileName + "'")

	return &newConfigFile
}

// Open create an new empty config File-Object
//
// This don't read the file itselfe, you need to call File.Read() after it
func Open(fullFileName string) *File {

	var newConfigFile = File{
		log: logrus.WithFields(
			logrus.Fields{
				"prefix": "CONFIG",
			},
		),
		fileName: fullFileName,
		entrys:   make(map[string]interface{}),
	}

	// debug
	newConfigFile.log.Debug("Config-File is '" + newConfigFile.fileName + "'")

	return &newConfigFile
}

// Read will read the config to internal memory
//
// Get it with GetJSONObject
func (c *File) Read() error {

	// Open our jsonFile
	jsonFile, err := os.Open(c.fileName)
	if err != nil {
		// okay non existant, create a new one
		c.Save()
		return nil
	}
	defer jsonFile.Close()

	c.log.Debug("Successfully Opened '" + c.fileName + "'")
	byteValue, _ := ioutil.ReadAll(jsonFile)
	return json.Unmarshal(byteValue, &c.entrys)
}

// ReadAES will load an AES-GCM decrypted config-file to internal memory
func (c *File) ReadAES(key []byte) error {

	var err error
	var ciphertext []byte
	var cipherBlock cipher.Block

	ciphertext, err = ioutil.ReadFile(c.fileName)
	if err != nil {
		c.log.Error(err)
		return err
	}

	cipherBlock, err = aes.NewCipher(key)
	if err != nil {
		c.log.Error(err)
		return err
	}

	gcm, err := cipher.NewGCM(cipherBlock)
	if err != nil {
		c.log.Error(err)
		return err
	}

	nonceSize := gcm.NonceSize()
	if len(ciphertext) < nonceSize {
		newError := errors.New("ciphertext < nonceSize")
		c.log.Error(newError)
		return newError
	}

	nonce, ciphertext := ciphertext[:nonceSize], ciphertext[nonceSize:]
	plaintext, err := gcm.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		c.log.Error(err)
		return err
	}

	c.log.Debug("Successfully Opened '" + c.fileName + "'")
	err = json.Unmarshal(plaintext, &c.entrys)
	return err
}

// GetJSONObject Return an json object
func (c *File) GetJSONObject(name string) (interface{}, error) {

	// then we try to get the object
	if jsonObject, ok := c.entrys[name].(interface{}); ok {
		return jsonObject, nil
	}

	return nil, fmt.Errorf("No Object found with name '%s'", name)
}

// SetJSONObject set an json object
func (c *File) SetJSONObject(name string, jsonNode interface{}) error {

	// save it
	c.entrys[name] = jsonNode

	return nil
}

// Save save you config to the core.json
func (c *File) Save() {
	byteValue, _ := json.MarshalIndent(c.entrys, "", "    ")
	err := ioutil.WriteFile(c.fileName, byteValue, 0644)
	if err != nil {
		c.log.Error(err.Error())
		os.Exit(-1)
	}
}

// SaveAES will write the internal memory to a file as AES-GCM encrypted file
func (c *File) SaveAES(key []byte) error {

	var err error
	var ciphertext []byte
	var cipherBlock cipher.Block
	var plaintext []byte

	// create our text
	plaintext, err = json.MarshalIndent(c.entrys, "", "    ")

	// generate a new aes cipher using our 32 byte long key
	cipherBlock, err = aes.NewCipher(key)
	// if there are any errors, handle them
	if err != nil {
		fmt.Println(err)
	}

	// gcm or Galois/Counter Mode, is a mode of operation
	// for symmetric key cryptographic block ciphers
	// - https://en.wikipedia.org/wiki/Galois/Counter_Mode
	gcm, err := cipher.NewGCM(cipherBlock)
	// if any error generating new GCM
	// handle them
	if err != nil {
		fmt.Println(err)
	}

	// creates a new byte array the size of the nonce
	// which must be passed to Seal
	nonce := make([]byte, gcm.NonceSize())
	// populates our nonce with a cryptographically secure
	// random sequence
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		fmt.Println(err)
	}

	// here we encrypt our text using the Seal function
	// Seal encrypts and authenticates plaintext, authenticates the
	// additional data and appends the result to dst, returning the updated
	// slice. The nonce must be NonceSize() bytes long and unique for all
	// time, for a given key.
	ciphertext = gcm.Seal(nonce, nonce, plaintext, nil)

	// write it to a file
	err = ioutil.WriteFile(c.fileName, ciphertext, 0644)
	if err != nil {
		c.log.Error(err.Error())
		return err
	}

	return nil
}
