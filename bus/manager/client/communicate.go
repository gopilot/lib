package client

import (
	"fmt"
	"net/url"

	"gitlab.com/gopilot/lib/bus/connection"
	"gitlab.com/gopilot/lib/bus/manager"
	"gitlab.com/gopilot/lib/bus/transport"
	"gitlab.com/gopilot/lib/bus/transport/message"
	"gitlab.com/gopilot/lib/bus/transport/socket"
)

// Communicate [BLOCKING] will start the TCP-Client and Blocks until an error occure
func (svr *Type) Communicate(cb manager.Callbacks) error {

	// parse the url
	u, err := url.Parse(svr.connectionURL)
	if err != nil {
		return err
	}

	// create a new transport
	var transport transport.Interface
	switch u.Scheme {
	case "unix":
		transport, err = socket.New(svr.connectionURL)
		if err != nil {
			return err
		}
	default:
		return fmt.Errorf("we can not handle scheme '%s'", u.Scheme)
	}

	// Handle message for the connection list
	recChan := make(chan connection.Event, 20)
	go svr.HandleMessages(recChan, make(chan bool, 1))

	// create a connection for this transport
	newClientConnection, _ := connection.New(transport)
	newClientConnection.PatternAdd("SUBREQ")
	newClientConnection.PatternAdd("HELO")
	newClientConnection.PatternAdd("OLEH")

	// wait for connections
	for {

		// wait for client
		svr.zlog.Info().Msg("Try to connect")
		newClientConnection.Connect()

		// wait for HELO
		svr.zlog.Debug().Msg("### Handshake START")
		msg, err := newClientConnection.MessageRecv()
		if msg.Topic != "HELO" || err != nil {
			newClientConnection.Disconnect()
			continue
		}

		// set connection id
		newClientConnection.IDSet(msg.Payload)

		// handshake
		newClientConnection.MessageSend(message.Msg{
			Topic:   "OLEH",
			Payload: newClientConnection.IDGet(),
		})
		svr.zlog.Debug().Msg("### Handshake DONE")

		// as client we wait for the subscription-request ( even if its empty )
		svr.zlog.Debug().Msg("### SUBREQ START")
		event, err := newClientConnection.EventGet()
		if event.Msg.Topic != "SUBREQ" || err != nil {
			svr.zlog.Error().Err(err).Msg("Dont get an SUBREQ")
			newClientConnection.Disconnect()
			continue
		}
		// send our subscriptions
		newClientConnection.SubscriptionSend(svr.GlobalSubscriptionsGet())
		svr.zlog.Debug().Msg("### SUBREQ DONE")

		// okay, now we can register our real channel
		svr.ConnectionAdd(newClientConnection)

		// callback
		if cb.OnConnected != nil {
			cb.OnConnected()
		}

		// run communication
		newClientConnection.Run(recChan)

	}

}
