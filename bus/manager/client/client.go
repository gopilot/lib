package client

import (
	"github.com/rs/zerolog/log"
	connectionlist "gitlab.com/gopilot/lib/bus/manager/connectionList"
	"gitlab.com/gopilot/lib/bus/transport/message"

	"github.com/rs/zerolog"
)

// Type represents a client-object
type Type struct {
	zlog             zerolog.Logger
	connectionURL    string
	messagesReceived chan message.Msg

	connectionlist.Type
}

// New create a new client
func New(connectionURL string) *Type {
	var newServer Type

	// logger
	newServer.zlog = log.Logger.With().
		Str("type", "client").
		Logger()

	// connection list
	newServer.ConnectionListInit()

	newServer.connectionURL = connectionURL
	return &newServer
}
