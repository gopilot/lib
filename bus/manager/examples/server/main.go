package main

import (
	"time"

	"gitlab.com/gopilot/lib/bus/manager/server"
	"gitlab.com/gopilot/lib/bus/transport/message"
)

var serverString = "unix:///tmp/TestClientDisconect.sock"

func main() {
	// server
	server := server.New(serverString)
	server.OnMessage("local", "test", func(connectionID string, msg message.Msg) {
		if msg.Topic == "local/test/ping" {
			time.Sleep(time.Millisecond * 100)
		}
	})
	server.Communicate()
}
