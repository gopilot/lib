package main

import (
	"time"

	"gitlab.com/gopilot/lib/bus/manager/client"
)

var serverString = "unix:///tmp/TestClientDisconect.sock"

func main() {

	client1 := client.New(serverString)
	go client1.Communicate()
	time.Sleep(time.Second * 3)

	client1.Send("local", "test", "ping", "")
	time.Sleep(time.Second * 10)

	client1.Send("local", "tested", "ping", "")
	time.Sleep(time.Second * 10)
}
