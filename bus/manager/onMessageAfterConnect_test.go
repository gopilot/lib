package manager

import (
	"os"
	"sync"
	"testing"
	"time"

	"github.com/rs/zerolog/log"

	"gitlab.com/gopilot/lib/bus/manager/client"
	"gitlab.com/gopilot/lib/bus/transport/message"

	"gitlab.com/gopilot/lib/bus/manager/server"

	"github.com/rs/zerolog"
)

func Test_OnMessage_AfterConnected(t *testing.T) {
	zerolog.SetGlobalLevel(zerolog.DebugLevel)
	log.Logger = zerolog.New(zerolog.ConsoleWriter{Out: os.Stdout}).With().Timestamp().Caller().Logger()

	var messagesSend sync.Mutex
	messagesSend.Lock()

	// server
	server := server.New(serverString)
	go server.Communicate()
	time.Sleep(time.Second * 1)

	// client
	client1 := client.New(serverString)
	go client1.Communicate()
	time.Sleep(time.Second * 1)

	server.OnMessage("local", "test", func(connectionID string, msg message.Msg) {
		if msg.Topic == "local/test/ping" {
			messagesSend.Unlock()
		}
	})
	time.Sleep(time.Second * 3)

	client1.Send("local", "test", "ping", "")
	messagesSend.Lock()
}
