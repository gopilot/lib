package manager

type Interface interface {
}

// Callbacks represents the callbacks for connection
type Callbacks struct {
	// OnHandshakeFinished will be called when the handshake is finished
	OnConnected func()
}
