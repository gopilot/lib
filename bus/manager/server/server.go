package server

import (
	"github.com/rs/zerolog/log"

	"github.com/rs/zerolog"
	connectionlist "gitlab.com/gopilot/lib/bus/manager/connectionList"
	"gitlab.com/gopilot/lib/bus/transport/message"
)

// Type represents a server-object
type Type struct {
	zlog             zerolog.Logger
	connectionURL    string
	messagesReceived chan message.Msg

	connectionlist.Type
}

// New create a new Server
func New(connectionURL string) *Type {
	var newServer Type

	// logger
	newServer.zlog = log.Logger.With().
		Str("type", "server").
		Logger()

	// connection list
	newServer.ConnectionListInit()

	newServer.connectionURL = connectionURL
	return &newServer
}
