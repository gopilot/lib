package server

import (
	"fmt"
	"net/url"

	"gitlab.com/gopilot/lib/bus/connection"
	"gitlab.com/gopilot/lib/bus/manager"
	"gitlab.com/gopilot/lib/bus/transport"
	"gitlab.com/gopilot/lib/bus/transport/message"
	"gitlab.com/gopilot/lib/bus/transport/socket"
)

// Communicate [BLOCKING] will start the TCP-Server
//
// Every new connection will run as an go-routine in the background
func (svr *Type) Communicate(cb manager.Callbacks) error {

	// parse the url
	u, err := url.Parse(svr.connectionURL)
	if err != nil {
		return err
	}

	// create a new transport
	var transport transport.Interface
	switch u.Scheme {
	case "unix":
		transport, err = socket.New(svr.connectionURL)
		if err != nil {
			return err
		}
	default:
		return fmt.Errorf("we can not handle scheme '%s'", u.Scheme)
	}

	// create a connection for this transport
	serverConnection, _ := connection.New(transport)
	serverConnection.IDGenerate()

	// serve
	svr.zlog.Info().Msg("Start Server")
	serverConnection.Serve()

	recChan := make(chan connection.Event, 20)
	go svr.HandleMessages(recChan, make(chan bool, 1))

	// wait for connections
	for {

		// wait for client
		svr.zlog.Info().Msg("Wait for client")
		newClientTransport, _ := serverConnection.WaitForClient()
		newClientConnection, _ := connection.New(newClientTransport)
		newClientConnection.IDGenerate()
		newClientConnection.PatternAdd("SUBREQ")
		newClientConnection.PatternAdd("HELO")
		newClientConnection.PatternAdd("OLEH")

		// handshake
		svr.zlog.Debug().Msg("### Handshake START")
		newClientConnection.MessageSend(message.Msg{
			Topic:   "HELO",
			Payload: newClientConnection.IDGet(),
		})

		// wait for OLEH
		msg, err := newClientConnection.MessageRecv()
		if msg.Topic != "OLEH" || err != nil {
			newClientConnection.Disconnect()
			continue
		}
		svr.zlog.Debug().Msg("### Handshake DONE")

		// send our subscriptions
		svr.zlog.Debug().Msg("### SUBREQ START")
		newClientConnection.SubscriptionSend(svr.GlobalSubscriptionsGet())
		// as client we wait for the subscription-request ( even if its empty )
		event, err := newClientConnection.EventGet()
		if event.Msg.Topic != "SUBREQ" || err != nil {
			svr.zlog.Error().Err(err).Msg("Dont get an SUBREQ")
			newClientConnection.Disconnect()
			continue
		}
		svr.zlog.Debug().Msg("### SUBREQ DONE")

		// we cann now start to write events to the event-channel
		go newClientConnection.Run(recChan)

		// okay, now we can register our real channel
		svr.ConnectionAdd(newClientConnection)

		// callback
		if cb.OnConnected != nil {
			cb.OnConnected()
		}
	}

}
