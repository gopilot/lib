package manager

import (
	"os"
	"sync"
	"testing"
	"time"

	"github.com/rs/zerolog/log"

	"gitlab.com/gopilot/lib/bus/manager/client"

	"gitlab.com/gopilot/lib/bus/manager/server"

	"github.com/rs/zerolog"
)

var serverString = "unix:///tmp/TestClientDisconect.sock"

func Test_Subscribe_OnExistingGroup(t *testing.T) {
	zerolog.SetGlobalLevel(zerolog.DebugLevel)
	log.Logger = zerolog.New(zerolog.ConsoleWriter{Out: os.Stdout}).With().Timestamp().Caller().Logger()

	var messagesSend sync.Mutex
	messagesSend.Lock()

	// server
	server := server.New(serverString)
	go server.Communicate()
	time.Sleep(time.Millisecond * 300)

	// client
	client := client.New(serverString)
	go client.Communicate()
	time.Sleep(time.Millisecond * 300)

}
