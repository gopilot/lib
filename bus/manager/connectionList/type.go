package connectionlist

import (
	"gitlab.com/gopilot/lib/bus/connection"
)

// Type represents the connectionlist-type
type Type struct {
	connectionList map[string]*connection.Type

	// patternSendFilters holds all pattern of all know subscriptions
	patternSendFilters map[string]byte
}
