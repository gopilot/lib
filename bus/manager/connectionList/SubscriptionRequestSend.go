package connectionlist

import (
	"encoding/json"

	"gitlab.com/gopilot/lib/bus/transport/message"
)

// SubscriptionRequestSend send a subscription-request with the global subsrc to all connections
func (common Type) SubscriptionRequestSend() {

	// map -> array
	var subscrArray []string
	for subscr := range common.patternSendFilters {
		subscrArray = append(subscrArray, subscr)
	}

	// array -> json
	subscrArrayJSON, _ := json.Marshal(subscrArray)

	// and send it to all known connections, so they can send us this info
	for _, connection := range common.connectionList {

		// send it
		connection.MessageSend(
			message.Msg{
				Topic:   "SUBREQ",
				Payload: string(subscrArrayJSON),
			},
		)

	}

}

// GlobalSubscriptionsGet return the subscriptions known by this manager
func (common Type) GlobalSubscriptionsGet() []string {
	// map -> array
	var subscrArray []string
	for subscr := range common.patternSendFilters {
		subscrArray = append(subscrArray, subscr)
	}

	return subscrArray
}
