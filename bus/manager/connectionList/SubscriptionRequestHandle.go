package connectionlist

import (
	"encoding/json"

	"gitlab.com/gopilot/lib/bus/connection"
)

// SubscriptionRequestHandle check if an subscription-request occured
//
// If message-topic contains SUBREQ , payload will be added to subscriptions of current connection
//
// return:
//
// - true if message-topic contains SUBREQ
//
// - false otherwise
func (common Type) SubscriptionRequestHandle(event connection.Event) bool {

	if event.Msg.Topic == "SUBREQ" {

		// json -> array
		var subscrArray []string
		json.Unmarshal([]byte(event.Msg.Payload), &subscrArray)

		// add subscriptions to this connections
		for _, subscr := range subscrArray {
			event.Con.PatternAdd(subscr)
		}

		return true
	}

	return false
}
