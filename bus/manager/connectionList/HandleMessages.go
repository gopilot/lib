package connectionlist

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/gopilot/lib/bus/connection"
)

// HandleMessages [BLOCKING]
func (conlist Type) HandleMessages(eventReceived chan connection.Event, exit chan bool) {

	for {
		select {
		case event := <-eventReceived:

			// send it to all connections
			conlist.SendMessage(*event.Msg)

		case <-exit:
			log.Debug().Msg("Stop handling messages")
		}
	}
}
