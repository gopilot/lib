package connectionlist

import (
	"fmt"

	"gitlab.com/gopilot/lib/bus/connection"

	"gitlab.com/gopilot/lib/bus/transport/callback"
	"gitlab.com/gopilot/lib/bus/transport/message"
)

// OnMessage will register an callback function
func (common Type) OnMessage(nodeName, groupName string, OnMessage func(connectionID string, msg message.Msg)) {

	// build the subscription-topic
	var subscptopic = ""
	if groupName == "" {
		subscptopic = nodeName + "/*"
	} else {
		subscptopic = fmt.Sprintf("%s/%s/*", nodeName, groupName)
	}

	// we add this pattern to global filter
	common.patternSendFilters[subscptopic] = 0
	common.SubscriptionRequestSend()

	// create an callback transport
	newCallbackTransport, _ := callback.New(OnMessage)
	newCallbackConnection, _ := connection.New(newCallbackTransport)
	newCallbackConnection.IDSet("callback")
	newCallbackConnection.PatternAdd(subscptopic)

	common.ConnectionAdd(newCallbackConnection)

}
