package connectionlist

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/gopilot/lib/bus/connection"
)

// ConnectionListInit will create the needed memory
func (common *Type) ConnectionListInit() {
	common.connectionList = make(map[string]*connection.Type)
	common.patternSendFilters = make(map[string]byte)
}

// ConnectionAdd will add an connection to the list
func (common Type) ConnectionAdd(con *connection.Type) {

	conID := con.IDGet()

	if conID == "" {
		log.Error().Msg("Can not add connection without an connection-id")
		return
	}

	log.Debug().Str("conID", conID).Msg("Add connection to the list")
	common.connectionList[conID] = con
}
