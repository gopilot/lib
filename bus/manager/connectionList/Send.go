package connectionlist

import (
	"fmt"

	"gitlab.com/gopilot/lib/bus/transport/message"
)

// Send will send a message over all connections where the pattern matches
func (common Type) Send(nodeName, groupName, command, payload string) {

	// build the subscription-topic
	fullTopic := fmt.Sprintf("%s/%s/%s", nodeName, groupName, command)

	// send it to all connections
	for _, connection := range common.connectionList {
		connection.MessageSend(
			message.Msg{
				Topic:   fullTopic,
				Payload: payload,
			},
		)
	}

}

// SendMessage send a msg to all connections
func (common Type) SendMessage(msg message.Msg) {
	// send it to all connections
	for _, connection := range common.connectionList {
		connection.MessageSend(msg)
	}
}
