package connection

import (
	"encoding/json"

	"gitlab.com/gopilot/lib/bus/transport/message"
)

// EventGet [BLOCKING] return an single event from the transport
//
// return nil if an error occure
func (c *Type) EventGet() (Event, error) {

	// wait for a message
	msg, err := c.transport.MessageRecv()
	msg.TypeSet(message.MsgTypeIn)

	// an error occured
	if err != nil {
		c.zlog.Error().Err(err).Msg("Could not read Message")
		return Event{}, err
	}

	// handle SUBREQ
	if msg.Topic == "SUBREQ" {

		// json -> array
		var subscrArray []string
		json.Unmarshal([]byte(msg.Payload), &subscrArray)

		// add subscriptions to this connections
		for _, subscr := range subscrArray {
			c.PatternAdd(subscr)
		}
	}

	// message received
	return Event{
		Con: c,
		Msg: &msg,
	}, nil

}
