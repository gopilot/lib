package connection

import (
	"encoding/json"

	"gitlab.com/gopilot/lib/bus/transport/message"
)

func (c *Type) SubscriptionSend(subscriptions []string) {

	// array -> json
	subscrArrayJSON, _ := json.Marshal(subscriptions)

	// and send it to all known connections, so they can send us this info

	// send it
	c.MessageSend(
		message.Msg{
			Topic:   "SUBREQ",
			Payload: string(subscrArrayJSON),
		},
	)

}
