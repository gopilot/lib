package connection

// PatternAdd will add an pattern
func (c *Type) PatternAdd(pattern string) {
	c.zlog.Debug().Str("pattern", pattern).Msg("Add pattern")
	c.patternSendFilters[pattern] = 0
}

// PatternGet return an array of pattern this connection is listening
func (c Type) PatternGet() []string {

	var newArray []string
	for pattern := range c.patternSendFilters {
		newArray = append(newArray, pattern)
	}

	return newArray
}
