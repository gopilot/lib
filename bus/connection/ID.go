package connection

import (
	"github.com/rs/zerolog/log"

	"github.com/google/uuid"
)

// IDGet return the current id of the message
func (con Type) IDGet() string {
	return con.id
}

// IDSet return the current id of the message
func (con *Type) IDSet(id string) {
	if con.id == "" {
		con.id = id

		con.zlog = log.Logger.With().
			Str("conID", id).
			Logger()

	}
}

// IDGenerate generate a new connection id ( only of not set before ! )
func (con *Type) IDGenerate() {
	// create an uuid
	uuidString := ""
	uuid, err := uuid.NewRandom()
	if err == nil {
		uuidString = uuid.String()
	}

	con.IDSet(uuidString)
}
