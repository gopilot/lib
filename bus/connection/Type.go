package connection

import (
	"github.com/rs/zerolog"
	"gitlab.com/gopilot/lib/bus/transport"
)

// Type represents a single connection
type Type struct {
	zlog zerolog.Logger

	// the connectionID
	id string

	// an pattern array
	patternSendFilters map[string]byte

	// the transport over we can communicate
	transport transport.Interface
}
