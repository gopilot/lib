package connection

import (
	"gitlab.com/gopilot/lib/bus/transport/message"
)

// Event is a single event that can occure on a connection
type Event struct {
	Con *Type
	Msg *message.Msg
}
