package connection

// Run [BLOCKING] will do the communication with the transport
func (c *Type) Run(eventReceived chan Event) {

	for {
		event, err := c.EventGet()
		if err == nil {
			eventReceived <- event
		}

	}

}
