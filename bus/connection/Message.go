package connection

import (
	"github.com/minio/minio/pkg/wildcard"
	"gitlab.com/gopilot/lib/bus/transport/message"
)

// MessageSend [BLOCKING] will send a message over the transport
//
// - the message-topic must match with a pattern in the connection
//
// - if pattern not match, we not transport the message
func (c Type) MessageSend(msg message.Msg) {

	sending := false

	// we check if a pattern match
	for pattern := range c.patternSendFilters {
		if wildcard.Match(pattern, msg.Topic) != true {
			continue
		}

		sending = true
		break
	}

	if sending == true {
		msg.LoggerAddInfos(c.zlog.Debug()).Msg("Pattern match, send over transport")
		c.transport.MessageSend(msg)
	} else {
		msg.LoggerAddInfos(c.zlog.Debug()).Msg("No pattern matches, we not sended this message over this transport")
	}

}
