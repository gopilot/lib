package connection

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/gopilot/lib/bus/transport"
)

// New create a new connection with an underlaying transport
func New(curTransport transport.Interface) (*Type, error) {

	var newConnection Type

	// setup the rest
	newConnection.zlog = log.Logger
	newConnection.transport = curTransport

	newConnection.patternSendFilters = make(map[string]byte)

	return &newConnection, nil
}
