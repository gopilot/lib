package connection

import (
	"gitlab.com/gopilot/lib/bus/transport"
	"gitlab.com/gopilot/lib/bus/transport/message"
)

// Serve start an server
func (con Type) Serve() error {
	return con.transport.Serve()
}

// WaitForClient [BLOCKING] return a new connection when a client connects
func (con Type) WaitForClient() (transport.Interface, error) {
	return con.transport.WaitForClient()
}

// Connect start an server
func (con Type) Connect() error {
	return con.transport.Connect()
}

// Disconnect will close the current socket connection
func (con Type) Disconnect() {
	con.transport.Disconnect()
}

// MessageRecv [BLOCKING] Read a message from the socket
func (con Type) MessageRecv() (message.Msg, error) {
	return con.transport.MessageRecv()
}
