/*Package callback is part of gopilot
Copyright (C) 2020 by Martin Langlotz aka stackshadow

This file is part of gopilot, an rewrite of the copilot-project in go

gopilot is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, version 3 of this License

gopilot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with gopilot.  If not, see <http://www.gnu.org/licenses/>.
*/
package callback

import (
	"github.com/rs/zerolog"
	"gitlab.com/gopilot/lib/bus/transport"
	"gitlab.com/gopilot/lib/bus/transport/message"
)

// Transport represents a single connection
type Transport struct {
	zlog zerolog.Logger

	// socket-stuff
	id string

	// callback
	OnMessage func(id string, msg message.Msg)
}

// Serve start an server
func (con *Transport) Serve() error {
	return nil
}

// WaitForClient does nothing
func (con *Transport) WaitForClient() (transport.Interface, error) {
	return &Transport{}, nil
}

// Connect does nothing
func (con *Transport) Connect() error {
	return nil
}

// Disconnect does nothing
func (con *Transport) Disconnect() {
	return
}

// MessageRecv does nothing
func (con *Transport) MessageRecv() (message.Msg, error) {
	return message.Msg{}, nil
}

// MessageSend call the callback message
func (con *Transport) MessageSend(msg message.Msg) error {
	msg.LoggerAddInfos(con.zlog.Debug()).Msg("Call callback")
	con.OnMessage(con.id, msg)
	return nil
}

// TypeGet return the transport-type
func (con *Transport) TypeGet() uint8 {
	return 2
}
