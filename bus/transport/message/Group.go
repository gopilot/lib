package message

import (
	"strings"
)

// Group return group from the message or "" if it can not be parsed
func (curMessage Msg) Group() string {

	topicArray := strings.Split(curMessage.Topic, "/")
	if len(topicArray) >= 2 {
		return topicArray[1]
	}

	return ""
}
