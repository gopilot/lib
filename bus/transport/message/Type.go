package message

import (
	"fmt"
)

// MsgTypeBroadcast represent an message that is sended to all clients
const MsgTypeBroadcast uint8 = 1

// MsgTypeIn represent an incoming message
const MsgTypeIn uint8 = 2

// MsgTypeOut represent an outgoing message
const MsgTypeOut uint8 = 3

// TypeGet return the current type of the message
func (curMessage Msg) TypeGet() uint8 {
	return curMessage.msgtype
}

// TypeSet set the current type of the message
//
// An message type can only be set once !
func (curMessage *Msg) TypeSet(msgtype uint8) error {
	if curMessage.msgtype != 0 {
		return fmt.Errorf("The type '%d' for this message is already set", curMessage.msgtype)
	}

	// set the message type
	curMessage.msgtype = msgtype

	return nil
}

// TypeToString return the type as a string for logging purpose
func TypeToString(msgtype uint8) string {

	switch msgtype {
	case MsgTypeBroadcast:
		return "BROADCAST"
	case MsgTypeIn:
		return "INCOMING"
	case MsgTypeOut:
		return "OUTGOING"
	}

	return ""
}
