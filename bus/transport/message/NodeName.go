package message

import (
	"strings"
)

// NodeName return nodename from the message or "" if it can not be parsed
func (curMessage Msg) NodeName() string {

	topicArray := strings.Split(curMessage.Topic, "/")
	if len(topicArray) >= 1 {
		return topicArray[0]
	}

	return ""
}
