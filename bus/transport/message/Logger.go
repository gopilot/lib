package message

import (
	"github.com/rs/zerolog"
)

// LoggerAddInfos will add infos to an current zerolog-event and return the new event
func (curMessage *Msg) LoggerAddInfos(in *zerolog.Event) *zerolog.Event {

	payloadLen := len(curMessage.Payload)
	if payloadLen > 25 {
		payloadLen = 25
	}

	return in.
		Str("msgID", curMessage.id).
		Str("msgTopic", curMessage.Topic).
		Str("msgType", TypeToString(curMessage.msgtype)).
		Str("msgPayload", curMessage.Payload[0:payloadLen])
}
