package message

import (
	"strings"
)

// Command return command from the message or "" if it can not be parsed
func (curMessage Msg) Command() string {

	topicArray := strings.Split(curMessage.Topic, "/")
	if len(topicArray) >= 3 {
		return topicArray[2]
	}

	return ""
}
