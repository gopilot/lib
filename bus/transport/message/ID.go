package message

func (curMessage *Msg) IDSet(newID string) {

	if curMessage.id == "" {
		curMessage.id = newID
	}

}

func (curMessage *Msg) IDGet() string {
	return curMessage.id
}
