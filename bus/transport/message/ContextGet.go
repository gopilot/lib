package message

// ContextGet will get the context
func (curMessage *Msg) ContextGet() interface{} {
	return curMessage.context
}
