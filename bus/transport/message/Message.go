package message

// Msg represent a single message inside the bus
type Msg struct {

	// id is the ID of the message
	id string

	// context can only be set during creation of a Message
	context interface{}

	// Type of the message
	msgtype uint8

	// The topic
	Topic string `json:"t"`

	// payload
	Payload string `json:"v"`
}
