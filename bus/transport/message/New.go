package message

// New will create a new Message
func New(id string, context interface{}) Msg {
	return Msg{
		id:      id,
		context: context,
	}
}
