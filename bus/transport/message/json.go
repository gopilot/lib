package message

import (
	"encoding/json"
	"fmt"
)

// ToJSONByteArray will convert an message to an byte-array for sending it out
func (curMessage *Msg) ToJSONByteArray() ([]byte, error) {

	b, err := json.Marshal(curMessage)
	if err != nil {
		fmt.Println("error:", err)
	}

	return b, nil
}

// ToJSONString will convert an message to an json string
func (curMessage *Msg) ToJSONString() (string, error) {

	b, err := json.Marshal(curMessage)
	if err != nil {
		fmt.Println("error:", err)
	}

	return string(b), nil
}

// FromJSONString will convert an json-string to an message
//
// - convert jsonString to message
// - set the context to <context>
func FromJSONString(jsonString string, context interface{}) (Msg, error) {

	var newMessage Msg

	err := json.Unmarshal([]byte(jsonString), &newMessage)
	if err != nil {
		fmt.Println("error: ", err)
		return newMessage, err
	}

	// set the context
	newMessage.context = context

	return newMessage, nil
}
