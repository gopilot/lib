package transport

import "gitlab.com/gopilot/lib/bus/transport/message"

// Interface represents a transport
type Interface interface {
	TypeGet() uint8

	// [BLOCKING]
	Serve() error

	// [BLOCKING]
	WaitForClient() (Interface, error)

	Connect() error
	Disconnect()

	// MessageRecv [BLOCKING] This read a message from the transport
	MessageRecv() (message.Msg, error)

	// MessageSend [BLOCKING] This send the message over the transport
	MessageSend(msg message.Msg) error
}

// Type represents the transports
type Type uint8

// TransportTypeSocket is an socket-transport
const TransportTypeSocket Type = 1

// TransportTypeCallback is an callback-transport
const TransportTypeCallback Type = 2
