/*Package socket is part of gopilot
Copyright (C) 2020 by Martin Langlotz aka stackshadow

This file is part of gopilot, an rewrite of the copilot-project in go

gopilot is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, version 3 of this License

gopilot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with gopilot.  If not, see <http://www.gnu.org/licenses/>.
*/
package socket

import (
	"net"
	"os"
)

// Serve start an server
func (con *Transport) Serve() error {

	// remove socket if it already exists
	if err := os.RemoveAll(con.filename); err != nil {
		return err
	}

	var err error
	con.socketListener, err = net.Listen("unix", con.filename)
	if err != nil {
		return err
	}

	con.zlog.Debug().Str("socket", con.filename).Msg("Start server")

	return nil
}
