/*Package socket is part of gopilot
Copyright (C) 2020 by Martin Langlotz aka stackshadow

This file is part of gopilot, an rewrite of the copilot-project in go

gopilot is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, version 3 of this License

gopilot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with gopilot.  If not, see <http://www.gnu.org/licenses/>.
*/
package socket

import (
	"errors"
	"fmt"

	"gitlab.com/gopilot/lib/bus/transport/message"
)

// MessageSend will send a message over the socket connection
func (con *Transport) MessageSend(msg message.Msg) error {
	// connected ?
	if con.socket == nil {
		return errors.New("Not connected")
	}

	// we don't send messages that comes from us
	if msg.ContextGet() == con {
		msg.LoggerAddInfos(con.zlog.Debug()).Msg("We dont send to sender")
		return nil
	}

	// convert to string
	newMessageString, _ := msg.ToJSONString()

	// iterate id
	msg.IDSet(string(fmt.Sprint(con.lastMessageID)))
	con.lastMessageID = con.lastMessageID + 1

	msg.LoggerAddInfos(con.zlog.Debug()).Msg("Send Message")

	return con.SendRaw(newMessageString)
}
