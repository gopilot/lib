/*Package socket is part of gopilot
Copyright (C) 2020 by Martin Langlotz aka stackshadow

This file is part of gopilot, an rewrite of the copilot-project in go

gopilot is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, version 3 of this License

gopilot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with gopilot.  If not, see <http://www.gnu.org/licenses/>.
*/
package socket

import "encoding/binary"

// ReadMessage [BLOCKING] Read a message from the buffer
func (con *Transport) readRaw() (message string, err error) {

	// we read the length ( 4 Bytes for uint32 (4*8=32) )
	byteBuffer := make([]byte, 4)
	readBytes, readErr := con.socket.Read(byteBuffer)
	if readBytes != 4 || readErr != nil {
		return "", readErr
	}
	messageSizeInBytes := binary.LittleEndian.Uint32(byteBuffer)

	// we read the message in size of the bytes we already get from the last read
	byteBuffer = make([]byte, messageSizeInBytes)
	readBytes, readErr = con.socket.Read(byteBuffer)
	if uint32(readBytes) != messageSizeInBytes || readErr != nil {
		return "", readErr
	}

	// convert it to string
	messageString := string(byteBuffer)

	return messageString, nil
}
