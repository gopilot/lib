/*Package socket is part of gopilot
Copyright (C) 2020 by Martin Langlotz aka stackshadow

This file is part of gopilot, an rewrite of the copilot-project in go

gopilot is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, version 3 of this License

gopilot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with gopilot.  If not, see <http://www.gnu.org/licenses/>.
*/
package socket

import (
	"errors"

	"gitlab.com/gopilot/lib/bus/transport/message"
)

// ReplyMessage will reply to an existing message
func (con *Transport) ReplyMessage(msg message.Msg, topic string, payload string) error {

	// we dont reply with the same command
	if msg.Topic == topic {
		return errors.New("You can not send the same command you received")
	}

	return con.MessageSend(message.Msg{
		Topic:   topic,
		Payload: payload,
	})
}
