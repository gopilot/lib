/*Package socket is part of gopilot
Copyright (C) 2020 by Martin Langlotz aka stackshadow

This file is part of gopilot, an rewrite of the copilot-project in go

gopilot is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, version 3 of this License

gopilot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with gopilot.  If not, see <http://www.gnu.org/licenses/>.
*/
package socket

import (
	"fmt"

	"github.com/rs/zerolog/log"

	"net/url"

	"gitlab.com/gopilot/lib/bus/transport/message"
)

// New create a new connection
//
// - An internal connection-id will be created ( random uuid )
//
// - Send and receivebuffer will be created
func New(connectionURL string) (*Transport, error) {

	// sheme
	u, err := url.Parse(connectionURL)
	if err != nil {
		return nil, err
	}

	if u.Scheme != "unix" {
		return nil, fmt.Errorf("scheme is not unix")
	}

	// create a new connection
	con := Transport{
		zlog: log.Logger.With().
			Str("transport", "socket").
			Logger(),

		id:            "",
		filename:      u.Path,
		socket:        nil,
		recvBuffer:    make(chan message.Msg, 50),
		sendBuffer:    make(chan message.Msg, 50),
		lastMessageID: 1,
	}

	return &con, nil
}
