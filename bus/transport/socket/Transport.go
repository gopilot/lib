/*Package socket is part of gopilot
Copyright (C) 2020 by Martin Langlotz aka stackshadow

This file is part of gopilot, an rewrite of the copilot-project in go

gopilot is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, version 3 of this License

gopilot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with gopilot.  If not, see <http://www.gnu.org/licenses/>.
*/
package socket

import (
	"net"

	"github.com/rs/zerolog"
	"gitlab.com/gopilot/lib/bus/transport/message"
)

// Transport represents a single connection
type Transport struct {
	zlog zerolog.Logger

	// socket-stuff
	id             string
	filename       string
	socketListener net.Listener
	socket         net.Conn // our socket

	// raw-message string
	recvBuffer chan message.Msg
	sendBuffer chan message.Msg

	// message-stuff
	lastMessageID int

	// node infos
	remoteNodeName  string
	remoteNodeGroup string
}

// TypeGet return the transport-type
func (con *Transport) TypeGet() uint8 {
	return 1
}
