/*Package socket is part of gopilot
Copyright (C) 2020 by Martin Langlotz aka stackshadow

This file is part of gopilot, an rewrite of the copilot-project in go

gopilot is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, version 3 of this License

gopilot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with gopilot.  If not, see <http://www.gnu.org/licenses/>.
*/
package socket

import (
	"github.com/google/uuid"
)

// IDGenerate generate a new connection id ( only of not set before ! )
func (con *Transport) IDGenerate() {
	// create an uuid
	uuidString := ""
	uuid, err := uuid.NewRandom()
	if err == nil {
		uuidString = uuid.String()
	}

	con.IDSet(uuidString)
}

// IDSet set the connection id ( only of not set before ! )
func (con *Transport) IDSet(newID string) {
	if con.id != "" {
		con.zlog.Error().Msg("Connection-ID is already set, can not set it !")
		return
	}

	con.zlog = con.zlog.With().Str("conid", newID).Logger()
	con.id = newID
}

// IDGet return the connection-id
func (con *Transport) IDGet() string {
	return con.id
}
