/*Package socket is part of gopilot
Copyright (C) 2020 by Martin Langlotz aka stackshadow

This file is part of gopilot, an rewrite of the copilot-project in go

gopilot is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, version 3 of this License

gopilot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with gopilot.  If not, see <http://www.gnu.org/licenses/>.
*/
package socket

import (
	"encoding/binary"
)

// SendRaw will send a raw string over the socket-connection
func (con *Transport) SendRaw(message string) error {
	// get the size of the message
	// 4 Bytes, because uint32 = 4*8 = 32Bit
	byteBuffer := make([]byte, 4)
	binary.LittleEndian.PutUint32(byteBuffer, uint32(len(message)))

	// write it out
	writeBytes, writeErr := con.socket.Write(byteBuffer)
	if writeBytes != 4 || writeErr != nil {
		return writeErr
	}

	// send it
	writeBytes, writeErr = con.socket.Write([]byte(message))
	if writeBytes <= 0 || writeErr != nil {
		return writeErr
	}

	return nil
}
