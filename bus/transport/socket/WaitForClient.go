/*Package socket is part of gopilot
Copyright (C) 2020 by Martin Langlotz aka stackshadow

This file is part of gopilot, an rewrite of the copilot-project in go

gopilot is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, version 3 of this License

gopilot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with gopilot.  If not, see <http://www.gnu.org/licenses/>.
*/
package socket

import (
	"gitlab.com/gopilot/lib/bus/transport"
)

// WaitForClient [BLOCKING] return a new connection when a client connects
func (con *Transport) WaitForClient() (transport.Interface, error) {

	// create a new connection
	newConnection, err := New("unix://" + con.filename)
	if err != nil {
		return nil, err
	}

	// wait for client
	var sockerr error
	newConnection.socket, sockerr = con.socketListener.Accept()
	if sockerr != nil {
		return nil, err
	}

	return newConnection, nil
}
