/*Package socket is part of gopilot
Copyright (C) 2020 by Martin Langlotz aka stackshadow

This file is part of gopilot, an rewrite of the copilot-project in go

gopilot is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, version 3 of this License

gopilot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with gopilot.  If not, see <http://www.gnu.org/licenses/>.
*/
package socket

import (
	"fmt"

	"gitlab.com/gopilot/lib/bus/transport/message"
)

// MessageRecv [BLOCKING] Read a message from the socket
func (con *Transport) MessageRecv() (msg message.Msg, err error) {

	// read from socket
	jsonString, err := con.readRaw()
	if err != nil {
		return message.Msg{}, err

	}

	// convert from JSON
	newMessage, err := message.FromJSONString(jsonString, con)
	if err != nil {
		con.zlog.Error().Str("raw", jsonString).Stack().Err(err).Msg("")
		return message.Msg{}, err
	}

	// iterate id
	newMessage.IDSet(fmt.Sprint(con.lastMessageID))
	con.lastMessageID = con.lastMessageID + 1

	newMessage.LoggerAddInfos(con.zlog.Debug()).Msg("Get Message")

	return newMessage, nil
}
