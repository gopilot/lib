/*Package mynodename is part of gopilot
Copyright (C) 2020 by Martin Langlotz aka stackshadow

This file is part of gopilot, an rewrite of the copilot-project in go

gopilot is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, version 3 of this License

gopilot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with gopilot.  If not, see <http://www.gnu.org/licenses/>.
*/
package mynodename

import (
	"flag"
	"os"

	log "github.com/sirupsen/logrus"
)

// ParseCmdLine [DEPRECATED PLEASE USE FromEnvironment() and FromCommandLine()] parse your command line parameter to internal variables
func ParseCmdLine() {
	log.WithFields(log.Fields{
		"prefix": "DEPRECATED",
		"info":   "Not used anymore. Please use FromCommandLine()",
	}).Warn()

	// hostname
	hostname, err := os.Hostname()
	if err != nil {
		hostname = "unknown"
	}

	flag.StringVar(&NodeName, "nodename", hostname, "Set the name of this node")

}
