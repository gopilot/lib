module gitlab.com/gopilot/lib

go 1.14

require (
	github.com/fzipp/gocyclo v0.0.0-20150627053110-6acd4345c835 // indirect
	github.com/google/uuid v1.1.1
	github.com/mgutz/ansi v0.0.0-20200706080929-d51e80ef957d // indirect
	github.com/minio/minio v0.0.0-20200707152854-14885ef98d83
	github.com/rs/zerolog v1.19.0
	github.com/sirupsen/logrus v1.5.0
	github.com/x-cray/logrus-prefixed-formatter v0.5.2
	golang.org/x/lint v0.0.0-20200302205851-738671d3881b // indirect
	golang.org/x/tools v0.0.0-20200823205832-c024452afbcd // indirect
)
